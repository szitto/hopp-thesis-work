package hu.unideb.hopp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import hu.unideb.hopp.adapter.AdsListAdapter;
import hu.unideb.hopp.utils.ToastUtil;
import hu.unideb.hopp.utils.VolleySingleton;


/**
 * A simple {@link Fragment} subclass.
 */
public class AdsListFragment extends Fragment {

    ListView adsListView;

    JSONArray adsList;

    public AdsListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle("Böngészés");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final FrameLayout view = (FrameLayout) inflater.inflate(R.layout.fragment_ads_list, container, false);

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, VolleySingleton.getServerURL() + "/ads/", null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                adsList = response;

                List<JSONObject> list = new ArrayList<>();
                for(int i= 0 ; i < adsList.length() ; i++) {
                    try {
                        list.add(adsList.getJSONObject(i));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                adsListView = (ListView) view.findViewById(R.id.adsList);
                AdsListAdapter adsListAdapter = new AdsListAdapter(getContext(), R.layout.ads_list_item, list);
                adsListView.setAdapter(adsListAdapter);
                adsListView.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        JSONObject item = (JSONObject) adsListView.getItemAtPosition(i);

                        String adsId = "";
                        try {
                            adsId = item.get("_id").toString();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        AdsDetailsFragment adsDetailsFragment = new AdsDetailsFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("ITEM_ID", adsId);
                        adsDetailsFragment.setArguments(bundle);

                        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.main_fragment_container, adsDetailsFragment);
                        fragmentTransaction.commit();
                    }
                });
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ToastUtil.showToast(getContext(), "A szerver nem elérhető");
                Log.e("MainActivity", error.toString());
            }
        });

        VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonArrayRequest);

        return view;
    }

}
