package hu.unideb.hopp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hu.unideb.hopp.adapter.AdsListAdapter;
import hu.unideb.hopp.utils.ToastUtil;
import hu.unideb.hopp.utils.UserProfileSingleton;
import hu.unideb.hopp.utils.VolleySingleton;


/**
 * A simple {@link Fragment} subclass.
 */
public class AdsDetailsFragment extends Fragment {


    TextView username;
    TextView startLocation;
    TextView endLocation;
    TextView date;
    TextView price;
    TextView space;
    TextView desc;
    Button singUp;

    public AdsDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle("Hirdetés adatai");
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.main, menu);

        MenuItem actionEdit = menu.findItem(R.id.actionEdit);
        actionEdit.setVisible(false);
        MenuItem actionCancel = menu.findItem(R.id.actionCancel);
        actionCancel.setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.actionMap:
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_fragment_container, new MapsFragment());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        FrameLayout view = (FrameLayout) inflater.inflate(R.layout.fragment_ads_details, container, false);

        username = (TextView) view.findViewById(R.id.adsDetailsUser);
        startLocation = (TextView) view.findViewById(R.id.adsDetailsStartLocation);
        endLocation = (TextView) view.findViewById(R.id.adsDetailsEndLocation);
        date = (TextView) view.findViewById(R.id.adsDetailsDate);
        price = (TextView) view.findViewById(R.id.adsDetailsPrice);
        space = (TextView) view.findViewById(R.id.adsDetailsSpace);
        desc = (TextView) view.findViewById(R.id.adsDetailsDesc);

        singUp = (Button) view.findViewById(R.id.adsDetailsSignUp);
        singUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signUp();
            }
        });

        Bundle args = getArguments();
        String adsId = args.getString("ITEM_ID");

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, VolleySingleton.getServerURL() + "/ads/" + adsId, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    JSONObject responseObject = response.getJSONObject(0);

                    username.setText(responseObject.get("userKey").toString());
                    startLocation.setText(responseObject.get("startLocation").toString());
                    endLocation.setText(responseObject.get("endLocation").toString());

                    String rawDate = responseObject.get("departureTime").toString();
                    String dateString = rawDate.substring(0, 10).replace("-", ".");
                    String timeString = rawDate.substring(11, 16);
                    date.setText(dateString + " " + timeString);

                    price.setText(responseObject.get("price").toString());
                    space.setText(responseObject.get("availableSpaces").toString());
                    desc.setText(responseObject.get("content").toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ToastUtil.showToast(getContext(), "A szerver nem elérhető");
                Log.e("MainActivity", error.toString());
            }
        });

        VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonArrayRequest);

        return view;
    }

    private void signUp() {
        ToastUtil.showToast(getContext(), "Sikeres jelentkezés.");

        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_fragment_container, new AdsListFragment());
        fragmentTransaction.commit();
    }
}
