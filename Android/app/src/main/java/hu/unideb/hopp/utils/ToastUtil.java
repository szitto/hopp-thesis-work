package hu.unideb.hopp.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by szentpeteri on 2015.11.30..
 */
public class ToastUtil {

    public static void showToast(Context ctx, String message) {
        Toast toast = Toast.makeText(ctx, message, Toast.LENGTH_SHORT);
        toast.show();
    }
}
