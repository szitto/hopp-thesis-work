package hu.unideb.hopp.utils;

import android.content.Context;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;
import java.util.Objects;

/**
 * Created by szentpeteri on 2015.12.03..
 */
public class UserProfileSingleton {
    private static UserProfileSingleton instance;
    private static JSONObject userProfile;


    private UserProfileSingleton() {

    }

    public static synchronized UserProfileSingleton getInstance() {
        if (instance == null) {
            instance = new UserProfileSingleton();
        }
        return instance;
    }

    public void setUserProfile(JSONObject user) {
        userProfile = user;
    }

    public JSONObject getUserProfile() {
        return  userProfile;
    }

    public JSONArray getCars() {
        try {
            return (JSONArray) userProfile.get("cars");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new JSONArray();
    }
}
