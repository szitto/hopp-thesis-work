package hu.unideb.hopp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import hu.unideb.hopp.utils.MD5Generator;
import hu.unideb.hopp.utils.ToastUtil;
import hu.unideb.hopp.utils.VolleySingleton;

public class RegistrationFragment extends Fragment {

    EditText emailInput;
    EditText lastNameInput;
    EditText firstNameInput;
    EditText mobileInput;
    EditText passwordInput;

    public RegistrationFragment() {
        // Required empty public constructor
    }

    public void registration() {
        Map<String,String> nameMap = new HashMap<>();
        nameMap.put("first", firstNameInput.getText().toString());
        nameMap.put("last", lastNameInput.getText().toString());
        JSONObject nameObject = new JSONObject(nameMap);

        Map<String,String> requestMap = new HashMap<>();
        requestMap.put("_id", emailInput.getText().toString());
        requestMap.put("mobile", mobileInput.getText().toString());
        requestMap.put("password", MD5Generator.getMD5(passwordInput.getText().toString()));

        JSONObject requestObject = new JSONObject(requestMap);
        try {
            requestObject.put("name", nameObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONArray requestArray = new JSONArray();
        requestArray.put(requestObject);

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST, VolleySingleton.getServerURL() + "/user/", requestArray, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    JSONObject responseObject = response.getJSONObject(0);
                    if(responseObject.get("response") != null) {
                        ToastUtil.showToast(getContext(), responseObject.get("response").toString());
                        //Navigate back to the login page
                        getFragmentManager().popBackStackImmediate();
                    } else {
                        ToastUtil.showToast(getContext(), "Nem sikerült létrehozni a felhasználót!");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ToastUtil.showToast(getContext(), "A szerver nem elérhető");
                Log.e("LoginActivity", error.toString());
            }
        });

        VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonArrayRequest);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final FrameLayout view = (FrameLayout) inflater.inflate(R.layout.fragment_registration, container, false);

        emailInput = (EditText) view.findViewById(R.id.regEmail);
        lastNameInput = (EditText) view.findViewById(R.id.regLastName);
        firstNameInput = (EditText) view.findViewById(R.id.regFirstName);
        mobileInput = (EditText) view.findViewById(R.id.regMobile);
        passwordInput = (EditText) view.findViewById(R.id.regPassword);

        passwordInput.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    ScrollView scrollView = (ScrollView) view.findViewById(R.id.regScrollView);
                    scrollView.scrollTo(0, scrollView.getBottom());

                    InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getView().getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);
                    return true;
                }
                return false;
            }
        });

        Button registrationButton = (Button) view.findViewById(R.id.registrationRegistrationButton);
        registrationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registration();
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
