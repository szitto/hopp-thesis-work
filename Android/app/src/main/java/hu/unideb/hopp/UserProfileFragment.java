package hu.unideb.hopp;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hu.unideb.hopp.utils.MD5Generator;
import hu.unideb.hopp.utils.ToastUtil;
import hu.unideb.hopp.utils.UserProfileSingleton;
import hu.unideb.hopp.utils.VolleySingleton;


/**
 * A simple {@link Fragment} subclass.
 */
public class UserProfileFragment extends Fragment {

    TextView username;
    TextView name;
    TextView mobile;
    Button call;
    Button message;
    Button addCar;
    ListView carsList;

    JSONObject userProfile;
    JSONArray cars;

    public UserProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle("Profil");
        setHasOptionsMenu(true);

        //Get user profile
        userProfile = UserProfileSingleton.getInstance().getUserProfile();
        //Set cars array
        try {
            cars = (JSONArray) userProfile.get("cars");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.main, menu);

        MenuItem actionCancel = menu.findItem(R.id.actionCancel);
        actionCancel.setVisible(false);
        MenuItem actionMap = menu.findItem(R.id.actionMap);
        actionMap.setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.actionEdit:
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_fragment_container, new UserProfileEditFragment());
                fragmentTransaction.commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        FrameLayout view = (FrameLayout) inflater.inflate(R.layout.fragment_user_profile, container, false);

        //Get view elements
        username = (TextView) view.findViewById(R.id.profileUserName);
        name = (TextView) view.findViewById(R.id.profileName);
        mobile = (TextView) view.findViewById(R.id.profileMobile);
        call = (Button) view.findViewById(R.id.profileCall);
        message = (Button) view.findViewById(R.id.profileMessage);
        addCar = (Button) view.findViewById(R.id.profileAddCar);
        carsList = (ListView) view.findViewById(R.id.profileCarsList);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, createCarsStringArray());
        carsList.setAdapter(arrayAdapter);

        addCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_fragment_container, new CarAddFragment());
                fragmentTransaction.commit();
            }
        });

        try {
            setData();

            //Set buttons listener
            call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mobile.getText().toString()));
                    startActivity(callIntent);
                }
            });
            message.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent msgIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + mobile.getText().toString()));
                    startActivity(msgIntent);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Inflate the layout for this fragment
        return view;
    }

    private void setData() throws JSONException {
        JSONObject nameObject = (JSONObject) userProfile.get("name");
        String nameOfUser = nameObject.get("last").toString() + " " + nameObject.get("first").toString();

        username.setText(userProfile.get("_id").toString());
        name.setText(nameOfUser);
        mobile.setText(userProfile.get("mobile").toString());
    }

    private String[] createCarsStringArray() {
        List<String> result = new ArrayList<String>();

        for (int i = 0 ; i < cars.length() ; i++) {
            try {
                result.add(cars.getJSONObject(i).get("licensePlate").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        String[] resultArray = new String[result.size()];
        return result.toArray(resultArray);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        MenuItem actionEdit = menu.findItem(R.id.actionEdit);
        actionEdit.setVisible(true);
    }
}
