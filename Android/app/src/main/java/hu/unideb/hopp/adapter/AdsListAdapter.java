package hu.unideb.hopp.adapter;

import android.content.ClipData;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import hu.unideb.hopp.R;

/**
 * Created by szentpeteri on 2015.12.05..
 */
public class AdsListAdapter extends ArrayAdapter<JSONObject> {

    public AdsListAdapter(Context context, int resource) {
        super(context, resource);
    }

    public AdsListAdapter(Context context, int resource, List<JSONObject> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.ads_list_item, null);
        }

        JSONObject item = getItem(position);

        String titleString = "";
        try {
            titleString += item.get("startLocation").toString();
            titleString += " - ";
            titleString += item.get("endLocation").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (item != null) {
            TextView title = (TextView) v.findViewById(R.id.adsListTitle);
            TextView price = (TextView) v.findViewById(R.id.adsListPrice);

            if (title != null) {
                title.setText(titleString);
            }

            if (price != null) {
                try {
                    price.setText(item.get("price").toString() + " Ft");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        return v;
    }
}
