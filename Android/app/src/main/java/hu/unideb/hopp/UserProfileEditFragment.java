package hu.unideb.hopp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import hu.unideb.hopp.utils.MD5Generator;
import hu.unideb.hopp.utils.ToastUtil;
import hu.unideb.hopp.utils.UserProfileSingleton;
import hu.unideb.hopp.utils.VolleySingleton;


/**
 * A simple {@link Fragment} subclass.
 */
public class UserProfileEditFragment extends Fragment {
    EditText username;
    EditText lastName;
    EditText firstName;
    EditText mobile;
    EditText password;
    Button save;

    JSONObject userProfile;

    public UserProfileEditFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle("Profil szerkesztése");
        setHasOptionsMenu(true);

        //Get user profile
        userProfile = UserProfileSingleton.getInstance().getUserProfile();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.main, menu);

        MenuItem actionEdit = menu.findItem(R.id.actionEdit);
        actionEdit.setVisible(false);
        MenuItem actionMap = menu.findItem(R.id.actionMap);
        actionMap.setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.actionCancel:
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_fragment_container, new UserProfileFragment());
                fragmentTransaction.commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        FrameLayout view = (FrameLayout) inflater.inflate(R.layout.fragment_user_profile_edit, container, false);

        username = (EditText) view.findViewById(R.id.profileEditUsername);
        lastName = (EditText) view.findViewById(R.id.profileEditLastname);
        firstName = (EditText) view.findViewById(R.id.profileEditFirstName);
        mobile = (EditText) view.findViewById(R.id.profileEditPhoneNumber);
        password = (EditText) view.findViewById(R.id.profileEditPassword);

        save = (Button) view.findViewById(R.id.profileEditSave);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateUser();
            }
        });

        try {
            setData();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }

    private void setData() throws JSONException {
        JSONObject nameObject = (JSONObject) userProfile.get("name");

        username.setText(userProfile.get("_id").toString());
        lastName.setText(nameObject.get("last").toString());
        firstName.setText(nameObject.get("first").toString());
        mobile.setText(userProfile.get("mobile").toString());
    }

    public void updateUser() {
        Map<String,String> nameMap = new HashMap<>();
        nameMap.put("first", firstName.getText().toString());
        nameMap.put("last", lastName.getText().toString());
        JSONObject nameObject = new JSONObject(nameMap);

        Map<String,String> requestMap = new HashMap<>();
        requestMap.put("mobile", mobile.getText().toString());
        if(!password.getText().toString().isEmpty()) {
            requestMap.put("password", MD5Generator.getMD5(password.getText().toString()));
        }
        requestMap.put("modifyUser", new Date().toString());

        JSONObject requestObject = new JSONObject(requestMap);
        try {
            requestObject.put("name", nameObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, VolleySingleton.getServerURL() + "/user/" + username.getText().toString(), requestObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.get("response") != null) {
                        ToastUtil.showToast(getContext(), response.get("response").toString());

                        JsonArrayRequest userProfileRequest = new JsonArrayRequest(Request.Method.GET, VolleySingleton.getServerURL() + "/user/" + userProfile.get("_id").toString(), null, new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray responseArray) {
                                try {
                                    JSONObject responseObject = responseArray.getJSONObject(0);
                                    if(responseObject != null) {
                                        //Set user profile in the singleton
                                        UserProfileSingleton.getInstance().setUserProfile(responseObject);

                                        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                                        fragmentTransaction.replace(R.id.main_fragment_container, new UserProfileFragment());
                                        fragmentTransaction.commit();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                ToastUtil.showToast(getContext(), "A szerver nem elérhető");
                                Log.e("MainActivity", error.toString());
                            }
                        });

                        VolleySingleton.getInstance(getContext()).addToRequestQueue(userProfileRequest);
                    } else {
                        ToastUtil.showToast(getContext(), "Nem sikerült frissíteni az adatokat!");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ToastUtil.showToast(getContext(), "A szerver nem elérhető");
                Log.e("MainActivity", error.toString());
            }
        });

        VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
    }

}
