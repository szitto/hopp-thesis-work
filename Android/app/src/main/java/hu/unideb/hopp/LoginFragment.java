package hu.unideb.hopp;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import hu.unideb.hopp.utils.MD5Generator;
import hu.unideb.hopp.utils.ToastUtil;
import hu.unideb.hopp.utils.UserProfileSingleton;
import hu.unideb.hopp.utils.VolleySingleton;

/**
 * A placeholder fragment containing a simple view.
 */
public class LoginFragment extends Fragment {

    EditText usernameInput;
    EditText passwordInput;
    EditText serverUrlInput;

    public LoginFragment() {
        // Required empty public constructor
    }

    public void login() {
        String username = usernameInput.getText().toString();
        final String password = passwordInput.getText().toString();

        //Set serverURL
        VolleySingleton.setServerURL(serverUrlInput.getText().toString());

        if(!(username.isEmpty() || password.isEmpty())) {
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, VolleySingleton.getServerURL() + "/user/" + username, null, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    try {
                        JSONObject responseObject = response.getJSONObject(0);
                        if(responseObject.get("password").equals(MD5Generator.getMD5(password))) {
                            //Set user profile in the singleton
                            UserProfileSingleton.getInstance().setUserProfile(responseObject);
                            //Navigate to the main activity
                            Intent mainIntent = new Intent(getActivity(), MainActivity.class);
                            startActivity(mainIntent);
                        } else {
                            ToastUtil.showToast(getContext(), "Rossz felhasználónév vagy jelszó");
                        }
                    } catch (JSONException e) {
                        ToastUtil.showToast(getContext(), "Rossz felhasználónév vagy jelszó");
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ToastUtil.showToast(getContext(), "A szerver nem elérhető");
                    Log.e("LoginActivity", error.toString());
                }
            });

            VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonArrayRequest);
        } else {
            ToastUtil.showToast(getContext(), "A felhasználónév és jelszó mezők nem maradhatnak üresen!");
        }
    }

    public void navigateToRegistration() {
        RegistrationFragment registrationFragment = new RegistrationFragment();

        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.login_fragment_container, registrationFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        FrameLayout view = (FrameLayout) inflater.inflate(R.layout.fragment_login, container, false);

        usernameInput = (EditText) view.findViewById(R.id.loginUsername);
        usernameInput.getText().clear();
        usernameInput.setText("user@admin.com");
        passwordInput = (EditText) view.findViewById(R.id.loginPassword);
        passwordInput.getText().clear();
        passwordInput.setText("admin");
        serverUrlInput = (EditText) view.findViewById(R.id.loginServerUrl);


        passwordInput.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    login();
                    return true;
                }
                return false;
            }
        });

        Button loginButton = (Button) view.findViewById(R.id.loginLoginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });
        Button registrationButton = (Button) view.findViewById(R.id.loginRegistrationButton);
        registrationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToRegistration();
            }
        });

        return view;
    }
}
