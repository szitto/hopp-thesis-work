package hu.unideb.hopp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hu.unideb.hopp.utils.MD5Generator;
import hu.unideb.hopp.utils.ToastUtil;
import hu.unideb.hopp.utils.UserProfileSingleton;
import hu.unideb.hopp.utils.VolleySingleton;


/**
 * A simple {@link Fragment} subclass.
 */
public class AdsAddFragment extends Fragment {

    Fragment currentFragment = this;

    Spinner carSpinner;

    SeekBar seekBar;
    TextView seekBarCounter;

    EditText price;
    EditText desc;
    EditText departureDate;
    EditText departureTime;
    EditText startLocation;
    EditText endLocation;
    Button save;

    JSONArray cars;
    JSONObject userProfile;
    JSONObject selectedCar;

    String availableSpaces;

    public AdsAddFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle("Hirdetés feladása");

        //Get cars
        cars = UserProfileSingleton.getInstance().getCars();
        //Get user profile
        userProfile = UserProfileSingleton.getInstance().getUserProfile();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        FrameLayout view = (FrameLayout) inflater.inflate(R.layout.fragment_ads_add, container, false);

        List<String> carsSpinnerList = new ArrayList<>();
        for (int i = 0 ; i < cars.length(); i++) {
            try {
                carsSpinnerList.add(cars.getJSONObject(i).get("licensePlate").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        carSpinner = (Spinner) view.findViewById(R.id.addAdsCarSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, carsSpinnerList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        carSpinner.setAdapter(adapter);
        carSpinner.setOnItemSelectedListener(onItemSelectedListener);

        seekBarCounter = (TextView) view.findViewById(R.id.addAdsSpacesCounter);

        seekBar = (SeekBar) view.findViewById(R.id.addAdsSpaces);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                seekBarCounter.setText(Integer.toString(i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                availableSpaces = Integer.toString(seekBar.getProgress());
            }
        });

        price = (EditText) view.findViewById(R.id.addAdsPrice);
        desc = (EditText) view.findViewById(R.id.addAdsContent);


        departureDate = (EditText) view.findViewById(R.id.addAdsDepartureDate);
        departureDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getChildFragmentManager();
                DatePickerFragment datePickerFragment = new DatePickerFragment();
                datePickerFragment.setTargetFragment(currentFragment, DatePickerFragment.REQUEST_CODE);
                datePickerFragment.show(fragmentManager, "datePickerFragment");
            }
        });
        departureTime = (EditText) view.findViewById(R.id.addAdsDepartureTime);
        departureTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getChildFragmentManager();
                TimePickerFragment timePickerFragment = new TimePickerFragment();
                timePickerFragment.setTargetFragment(currentFragment, TimePickerFragment.REQUEST_CODE);
                timePickerFragment.show(fragmentManager, "timePickerFragment");
            }
        });

        startLocation = (EditText) view.findViewById(R.id.addAdsStartLocation);
        endLocation = (EditText) view.findViewById(R.id.addAdsEndLocation);

        save = (Button) view.findViewById(R.id.addAdsSave);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save();
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == DatePickerFragment.REQUEST_CODE) {
            String pickedDate = data.getStringExtra("pickedDate");
            departureDate.setText(pickedDate);
        }
        if(requestCode == TimePickerFragment.REQUEST_CODE) {
            String pickedHour = data.getStringExtra("pickedHour");
            String pickedMinute = data.getStringExtra("pickedMinute");
            departureTime.setText(pickedHour + ":" + pickedMinute);
        }
    }

    AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            for (int j = 0; j < cars.length(); j++) {
                try {
                    if (adapterView.getItemAtPosition(i).equals(cars.getJSONObject(i).get("licensePlate").toString())) {
                        try {
                            selectedCar = cars.getJSONObject(i);
                            seekBar.setMax(Integer.parseInt(selectedCar.get("space").toString()) -1);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    private String generateDepartureDate() {
        //2015.12.05 11:22
        String[] timeArray = departureTime.getText().toString().split("[:]+");

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd");

        Date date = null;
        try {
            date = simpleDateFormat.parse(departureDate.getText().toString());
            date.setHours(Integer.parseInt(timeArray[0]));
            date.setMinutes(Integer.parseInt(timeArray[1]));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date.toString();
    }

    private void save() {
        Map<String,String> requestMap = new HashMap<>();
        try {
            requestMap.put("userKey", userProfile.get("_id").toString());
            requestMap.put("carKey", selectedCar.get("licensePlate").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        requestMap.put("price", price.getText().toString());
        requestMap.put("content", desc.getText().toString());
        requestMap.put("availableSpaces", availableSpaces);
        requestMap.put("departureTime", generateDepartureDate());
        requestMap.put("startLocation", startLocation.getText().toString());
        requestMap.put("endLocation", endLocation.getText().toString());
;
        JSONObject requestObject = new JSONObject(requestMap);

        JSONArray requestArray = new JSONArray();
        requestArray.put(requestObject);

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST, VolleySingleton.getServerURL() + "/ads/", requestArray, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    JSONObject responseObject = response.getJSONObject(0);
                    if(responseObject.get("response") != null) {
                        ToastUtil.showToast(getContext(), responseObject.get("response").toString());

                        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.main_fragment_container, new AdsListFragment());
                        fragmentTransaction.commit();
                    } else {
                        ToastUtil.showToast(getContext(), "Nem sikerült létrehozni a hirdetést!");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ToastUtil.showToast(getContext(), "A szerver nem elérhető");
                Log.e("MainActivity", error.toString());
            }
        });

        VolleySingleton.getInstance(getContext()).addToRequestQueue(jsonArrayRequest);
    }
}
