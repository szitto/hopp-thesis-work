package hu.unideb.hopp.utils;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by szentpeteri on 2015.11.21..
 */
public class VolleySingleton {
    public static final String TAG = VolleySingleton.class.getSimpleName();

    private static final String defaultURL = "http://192.168.0.119:3000";
    private static String serverURL;
    private static VolleySingleton instance;
    private RequestQueue requestQueue;
    private static Context ctx;


   private VolleySingleton(Context context) {
       ctx = context;
       requestQueue = getRequestQueue();
   }

    public static synchronized VolleySingleton getInstance(Context context) {
        if (instance == null) {
            instance = new VolleySingleton(context);
        }
        return instance;
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(ctx.getApplicationContext());
        }
        return requestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        //Set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequest(Object tag) {
        if (requestQueue != null) {
            requestQueue.cancelAll(tag);
        }
    }

    public static void setServerURL(String url) {
        if(url.isEmpty()) {
            serverURL = defaultURL;
        } else {
            serverURL = url;
        }
    }

    public static String getServerURL() {
        return serverURL;
    }
}