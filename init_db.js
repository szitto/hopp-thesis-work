db = db.getSiblingDB('hopp');

db.users.remove({});

db.users.insert({
	_id: "user@admin.com",
	name: {
		first: "Admin",
		last: "User"
	},
	mobile: "+36700000000",
	password: "21232f297a57a5a743894a0e4a801fc3",
	cars: [],
	signedUpAds: [],
	createUser: new Date(),
	modifyUser: new Date()
});
