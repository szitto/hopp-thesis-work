/**
 * Created by szentpeteri on 2015.04.21..
 */

exports.create = function(model, document, callback, next) {
    model.create(document, function(err) {
        if (err) return next(err);
        if (typeof callback === "function") callback();
    });
};

exports.find = function(model, fields, callback, next) {
    model.find(fields, function(err, data) {
        if (err) return next(err);
        if (typeof callback === "function") callback(data);
    })
};

exports.findAll = function(model, callback, next) {
    model.find(function(err, data) {
        if (err) return next(err);
        if (typeof callback === "function") callback(data);
    })
};

exports.update = function(model, id, modifications, callback, next) {
    model.update(id, modifications, function(err) {
        if (err) return next(err);
        if (typeof callback === "function") callback();
    })
};

exports.remove = function(model, id, callback, next) {
    model.remove(id, function(err) {
        if (err) return next(err);
        if (typeof callback === "function") callback();
    });
};