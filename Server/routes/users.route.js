/**
 * Created by Nandor_Szentpeteri on 4/22/2015.
 */

var express = require('express');
var router = express.Router();
var operations = require('../operations');

//Initialize user model
var userModel = require('../models/users.model');

/**
 * CREATE /user
 */
router.post('/', function(req, res, next) {
   operations.create(userModel, req.body, function() {
       res.status(200).json([{"response": "'" + req.body[0]._id + "' user is successfully created!"}]);
   }, next);
});

/**
 * GET /user
 */
router.get('/', function(req, res, next) {
    operations.findAll(userModel, function(data) {
        res.json(data);
    }, next);
});

/**
 * GET /user/:id
 */
router.get('/:id', function (req, res, next) {
    operations.find(userModel, {_id: req.params.id}, function(data) {
        res.json(data);
    }, next);
});

/**
 * PUT /user/:id
 */
router.put('/:id', function(req, res, next) {
    operations.update(userModel, {_id: req.params.id}, req.body, function() {
        res.status(200).json({"response": "'" + req.params.id + "' user is successfully updated!"});
    }, next);
});

/**
 * DELETE /user/:id
 */
router.delete('/:id', function(req, res, next) {
    operations.remove(userModel, {_id: req.params.id}, function() {
        res.status(200).json([{"response": "'" + req.params.id + "' user is successfully deleted!"}]);
    }, next);
});

module.exports = router;