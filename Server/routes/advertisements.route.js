/**
 * Created by Nandor_Szentpeteri on 4/22/2015.
 */

var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var objectId = mongoose.Types.ObjectId;
var operations = require('../operations');

//Initialize user model
var adsModel = require('../models/advertisements.model');

/**
 * CREATE /ads
 */
router.post('/', function(req, res, next) {
    operations.create(adsModel, req.body, function() {
        res.status(200).json([{"response": "Advertisement is successfully created!"}]);
    }, next);
});

/**
 * GET /ads
 */
router.get('/', function(req, res, next) {
    operations.findAll(adsModel, function(data) {
        res.json(data);
    }, next);
});

/**
 * GET /ads/:id
 */
router.get('/:id', function (req, res, next) {
    operations.find(adsModel, {_id: objectId(req.params.id)}, function(data) {
        res.json(data);
    }, next);
});

/**
 * PUT /ads/:id
 */
router.put('/:id', function(req, res, next) {
    operations.update(adsModel, {_id: objectId(req.params.id)}, req.body, function() {
        res.status(200).json({"response": "'" + req.params.id + "' advertisement is successfully updated!"});
    }, next);
});

/**
 * DELETE /ads/:id
 */
router.delete('/:id', function(req, res, next) {
    operations.remove(adsModel, {_id: req.params.id}, function() {
        res.status(200).json([{"response": "'" + req.params.id + "' advertisement is successfully deleted!"}]);
    }, next);
});

module.exports = router;