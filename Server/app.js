/**
 * Created by szentpeteri on 2015.04.21..
 *
 * Used resources
 *      http://adrianmejia.com/blog/2014/10/01/creating-a-restful-api-tutorial-with-nodejs-and-mongodb/
 *      http://expressjs.com/starter/hello-world.html
 *      http://mongoosejs.com/docs/api.html#model_Model.update
 */

//Initialize dependencies
var express = require('express');
var bodyParser = require('body-parser');
var mongodb = require('mongodb');
var mongoose = require('mongoose');

//Create express app
var app = express();

//Configure app
app.use(bodyParser.json());

//Initialize routes
var user = require('./routes/users.route');
var ads = require('./routes/advertisements.route');

//Set routes
app.use('/user', user);
app.use('/ads', ads);

app.use('/', function(req, res) {
   res.send("hopp! node.js server is running!") 
});

//Handle database connection
var db = mongoose.connection;

//Connect to database
mongoose.connect('mongodb://localhost/hopp');

db.on('error', function() {
    console.error('[ERROR] Cannot connect to the database!\n');
});

db.once('open', function() {
    console.info('[INFO] Successfully connected to database!');
    
    //Start express server
    var server = app.listen(3000, function () {

    var host = server.address().address;
    var port = server.address().port;

    console.info('[INFO] hopp! server is listening at http://%s:%s', host, port);

    });
    
});