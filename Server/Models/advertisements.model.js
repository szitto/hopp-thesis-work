/**
 * Created by szentpeteri on 2015.04.21..
 */

var mongoose = require('mongoose');

//Create the advertisement schema
var advertisementSchema = new mongoose.Schema({
    userKey: String,
    carKey: String,
    price: String,
    content: String,
    availableSpaces: Number,
    departureTime: Date,
    startLocation: String,
    endLocation: String
}, {collection: 'ads'});

//Publish the model
var Ads = mongoose.model('ads', advertisementSchema);
module.exports = Ads;