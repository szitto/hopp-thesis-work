/**
 * Created by szentpeteri on 2015.04.21..
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Create the car schema
var carSchema = new mongoose.Schema({
    licensePlate: {type: String, required: true},
    manufacturer: String,
    type: String,
    space: Number
});

//Create the user schema
var userSchema = new mongoose.Schema({
    _id: {type: String, required: true},
    name: {first: String, last: String},
    mobile: {type: String, trim: false},
    password: String,
    cars: [carSchema],
    signedUpAds: [Schema.ObjectId],
    createUser: { type: Date, default: Date.now},
    modifyUser: { type: Date, default: Date.now}
}, {collection: 'users'});

//Publish the model
var Users = mongoose.model('users', userSchema);
module.exports = Users;